# gendl-patches

Source patches to be applied to currently deployed gendl/gdl builds
and merged into devo branch of gendl/gendl prior to merging with
master for next official build.
